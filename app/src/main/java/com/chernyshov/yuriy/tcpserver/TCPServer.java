package com.chernyshov.yuriy.tcpserver;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 09.10.14
 * Time: 17:05
 */
public class TCPServer extends Thread {

    public interface TCPServerListener {

        public void onMessageReceived(String message);

        public void onConnecting(String ipAddress, String port);
    }

    private static final String LOG_TAG = TCPServer.class.getSimpleName();
    private static final int SERVER_PORT = 4444;

    private boolean running = false;
    private TCPServerListener messageListener;
    private Context mContext;

    private PrintWriter mOut;

    public TCPServer(Context context, TCPServerListener messageListener) {
        this.messageListener = messageListener;
        mContext = context;
    }

    @Override
    public void run() {
        super.run();

        running = true;

        final WifiManager wm = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        messageListener.onConnecting(
                Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress()),
                String.valueOf(SERVER_PORT));

        try {
            Log.d(LOG_TAG, "Connecting...");

            //create a server socket. A server socket waits for requests to come in over the network.
            final ServerSocket serverSocket = new ServerSocket(SERVER_PORT);

            //create client socket... the method accept() listens for a connection to be made
            // to this socket and accepts it.
            final Socket client = serverSocket.accept();
            Log.d(LOG_TAG, "Receiving...");

            try {

                //sends the message to the client
                mOut = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(client.getOutputStream())), true);

                //read the message received from client
                final BufferedReader in = new BufferedReader(
                        new InputStreamReader(client.getInputStream()));

                //in this while we wait to receive messages from client (it's an infinite loop)
                //this while it's like a listener for messages
                while (running) {
                    final String message = in.readLine();

                    if (message != null && messageListener != null) {
                        //call the method onMessageReceived from ServerBoard class
                        messageListener.onMessageReceived(message);
                    }
                }

            } catch (Exception e) {
                Log.e(LOG_TAG, "Exception:" + e.getMessage());
            } finally {
                client.close();
                Log.d(LOG_TAG, "Done.");
            }

        } catch (Exception e) {
            Log.e(LOG_TAG, "Exception:" + e.getMessage());
        }
    }

    /**
     * Method to send the messages from server to client
     * @param message the message sent by the server
     */
    public void sendMessage(String message) {
        if (mOut != null && !mOut.checkError()) {
            mOut.println(message);
            mOut.flush();
        }
    }
}