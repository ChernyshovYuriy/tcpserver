package com.chernyshov.yuriy.tcpserver;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

    @SuppressWarnings("unused")
    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    private TCPServer mServer;
    private TextView mMessagesView;
    private TextView mIPView;
    private EditText mInputMessageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMessagesView = (TextView) findViewById(R.id.messages_view);
        mIPView = (TextView) findViewById(R.id.device_ip_view);
        mInputMessageView = (EditText) findViewById(R.id.input_message_view);
    }

    public void startServer(final View view) {
        Button startServer = (Button) view.findViewById(R.id.start_server_btn);

        // disable the start button
        startServer.setEnabled(false);

        //creates the object OnMessageReceived asked by the TCPServer constructor
        mServer = new TCPServer(this, new TCPServer.TCPServerListener() {

            @Override
            public void onConnecting(final String ipAddress, final String port) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mIPView.setText("IP:" + ipAddress + ", PORT:" + port);
                    }
                });
            }

            @Override
            //this method declared in the interface from TCPServer class is implemented here
            //this method is actually a callback method, because it will run every time when it will be called from
            //TCPServer class (at while)
            public void onMessageReceived(final String message) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMessagesView.append("\n-> " + message);
                    }
                });
            }
        });
        mServer.start();
    }

    public void sendMessage(View view) {
        // get the message from the text view
        final String messageText = mInputMessageView.getText().toString();
        // add message to the message area
        mMessagesView.append("\n<- " + messageText);
        // send the message to the client
        mServer.sendMessage(messageText);
        // clear text
        mInputMessageView.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
